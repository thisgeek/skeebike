# Based on https://kuenishi.github.io/memo/ergodox-ez.html
# Confirmed by https://kandepet.com/dissecting-the-ergodox-the-ergonomic-programmable-keyboard/
# Related to https://github.com/qmk/qmk_firmware/tree/master/keyboards/ergodox_eZ
# Untested
flash:
	teensy_loader_cli -mmcu=atmega32u4 -w ergodox_ez_base_skeebike_*.hex
